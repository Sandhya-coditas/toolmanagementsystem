const tHead = ["orderId", "customerName", "orderedTools"];
const orderId = document.getElementById('orderId');
const customerName = document.getElementById('customerName');
const orderedTools = document.getElementById('orderedTools');

const ordersTable = (token) => {
    const table = document.createElement('table');
    table.className = 'table';

    const tableHead = document.createElement('thead');
    tableHead.className = 'tableHead';

    const tableHeadRow = document.createElement('tr');
    tableHeadRow.className = 'tableHeadRow';

    tHead.forEach(header => {
        const currentHeader = document.createElement('th');
        currentHeader.innerText = header;
        tableHeadRow.append(currentHeader);
    });
    tableHead.append(tableHeadRow);
    table.append(tableHead);

    const tableBody = document.createElement('tbody');
    tableBody.className = "table-Body";
    table.append(tableBody);
    document.body.append(table);


    const getOrders = async () => {
        try {
            let response = await fetch("http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/getOrders", {
                method: "get",
                headers: new Headers({
                    "Authorization":"Bearer "+token,
                    "ngrok-skip-browser-warning": "3200",
                    'Content-Type': 'application/json',
                    'charset': 'utf-8',
                })
            });
            let data = await response.json();
            console.log(data);
            result(data,token);
        }
        catch (error) {
            console.log(error);
        }

    }
    getOrders();



    const result = (data) => {

        let template = "";
        const placeholder = document.getElementById("tbody");
        for (let index = 0; index < data.length; index++) {
            template +=
                `
                <h1>ORDERS MANAGEMENT SYSTEM</h1>

                <tr>
                         <td> ${data[index].orderId} </td>
                         <td> ${data[index].customerName} </td>
                         <td> ${data[index].orderedTools} </td>
                         <td><button class="editButton">Edit</button>
                         <button class="deleteButton">Delete</button></td>
                </tr>
     
     `
        }
        console.log(template);
        placeholder.innerHTML = template;

    }


}


export default ordersTable();










