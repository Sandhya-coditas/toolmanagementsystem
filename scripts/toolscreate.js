const form = document.createElement("form");
const createButton = document.getElementById("create");

create.addEventListener("click", function createForm() {

  const toolId = document.createElement("input");
  toolId.setAttribute("type", "number");
  toolId.setAttribute("value", "toolId");
  toolId.setAttribute("placeholder", "toolId");
  toolId.setAttribute("id", "toolId");

  const toolName = document.createElement("input");
  toolName.setAttribute("type", "text");
  toolName.setAttribute("name", "toolName");
  toolName.setAttribute("placeholder", "toolName");
  toolName.setAttribute("id", "toolName");

  const toolSize = document.createElement("input");
  toolSize.setAttribute("type", "text");
  toolSize.setAttribute("name", "toolSize");
  toolSize.setAttribute("placeholder", "toolSize");
  toolSize.setAttribute("id", "toolSize");


  const toolPrice = document.createElement("input");
  toolPrice.setAttribute("type", "text");
  toolPrice.setAttribute("name", "toolPrice");
  toolPrice.setAttribute("placeholder", "toolPrice");
  toolPrice.setAttribute("id", "toolPrice");

  const submit = document.createElement("input");
  submit.setAttribute("type", "submit");
  submit.setAttribute("value", "Submit");

  form.appendChild(toolId);
  form.appendChild(toolName);
  form.appendChild(toolSize);
  form.appendChild(toolPrice);
  form.appendChild(submit);


  document.body.appendChild(form);

  form.addEventListener("submit", async (e) => {

    e.preventDefault();
    const toolId = document.getElementById("toolId").value;
    const toolName= document.getElementById("toolName").value;
    const toolSize= document.getElementById("toolSize").value;
    const toolPrice = document.getElementById("toolPrice").value;
   

    try{

     fetch("https://0875-103-169-241-128.in.ngrok.io/admin/createTool", {
      method: "POST",
      headers: new Headers({
        "ngrok-skip-browser-warning": "3200",
        'contentType': 'application/json',
        charset: 'utf-8',
      }),

      body: JSON.stringify({
        'toolId': toolId,
        'toolName': toolName,
        'toolSize': toolSize,
        'toolPrice': toolPrice
      }),   
    });
  }
  catch(e){
    console.log(e);
  }


    try{
      fetch("https://0875-103-169-241-128.in.ngrok.io/admin/updateTool", {
        method: "PUT",
        headers: new Headers({
          "ngrok-skip-browser-warning": "3200",
          'contentType': 'application/json',
          charset: 'utf-8',
        }),
  
        body: JSON.stringify({
          'toolId': toolId,
          'toolName': toolName,
          'toolSize': toolSize,
          'toolPrice': toolPrice
        }),
      });
    } 
    catch(e){
      console.log(e);
    }
  });
});





