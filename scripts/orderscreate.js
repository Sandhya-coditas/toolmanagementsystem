const form = document.createElement("form");
const createButton = document.getElementById("create");

create.addEventListener("click", function createForm() {

    const orderId = document.createElement("input");
    orderId.setAttribute("type", "number");
    orderId.setAttribute("value", "orderId");
    orderId.setAttribute("placeholder", "orderId");
    orderId.setAttribute("id", "orderId");

    const customerName = document.createElement("input");
    customerName.setAttribute("type", "text");
    customerName.setAttribute("name", "customerName");
    customerName.setAttribute("placeholder", "customerName");
    customerName.setAttribute("id", "customerName");

    const orderedTools = document.createElement("input");
    orderedTools.setAttribute("type", "text");
    orderedTools.setAttribute("name", "orderedTools");
    orderedTools.setAttribute("placeholder", "orderedTools");
    orderedTools.setAttribute("id", "orderedTools");

    const submit = document.createElement("input");
    submit.setAttribute("type", "submit");
    submit.setAttribute("value", "Submit");

    form.appendChild(orderId);
    form.appendChild(customerName);
    form.appendChild(orderedTools);
    form.appendChild(submit);


    document.body.appendChild(form);

    form.addEventListener("submit", async (e) => {

        e.preventDefault();
        const orderId = document.getElementById("orderId").value;
        const customerName = document.getElementById("customerName").value;
        const orderedTools = document.getElementById("orderedTools").value;

        try{
            fetch("https://0875-103-169-241-128.in.ngrok.io/admin/createOrder", {
            method: "POST",
            headers: new Headers({
                "ngrok-skip-browser-warning": "3200",
                'contentType': 'application/json',
                charset: 'utf-8',
            }),

            body: JSON.stringify({
                'orderId': `${orderId}`,
                'customerName': `${customerName}`,
                'orderedTools': `${orderedTools}`,


            }),



        });
    }
    catch(e){
        console.log(e);
    }
      

    try{
        fetch("https://0875-103-169-241-128.in.ngrok.io/admin/updateOrder", {
            method: "PUT",
            headers: new Headers({
                "ngrok-skip-browser-warning": "3200",
                'contentType': 'application/json',
                charset: 'utf-8',
            }),

            body: JSON.stringify({
                'orderId': orderId,
                'customerName': customerName,
                'orderedTools': orderedTools,


            }),



        });
    }
    catch(e){
        console.log(e);
    }
});
});





