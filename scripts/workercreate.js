const form = document.createElement("form");
const createButton = document.getElementsByClassName("create");

create.addEventListener("click", function createForm() {

  form.setAttribute("method", "post");
  form.setAttribute("action", "http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/createWorker");

  const workerId = document.createElement("input");
  workerId.setAttribute("type", "number");
  workerId.setAttribute("value", "workerId");
  workerId.setAttribute("placeholder", "WorkerId");
  workerId.setAttribute("id", "workerId");

  const workerName = document.createElement("input");
  workerName.setAttribute("type", "text");
  workerName.setAttribute("name", "workerName");
  workerName.setAttribute("placeholder", "WorkerName");
  workerName.setAttribute("id", "workerName");

  const workerUsername = document.createElement("input");
  workerUsername.setAttribute("type", "text");
  workerUsername.setAttribute("name", "workerUsername");
  workerUsername.setAttribute("placeholder", "WorkerUsername");
  workerUsername.setAttribute("id", " workerUsername");


  const workerPassword = document.createElement("input");
  workerPassword.setAttribute("type", "text");
  workerPassword.setAttribute("name", "workerPassword");
  workerPassword.setAttribute("placeholder", "Worker Password");
  workerPassword.setAttribute("id", "workerPassword");


  const workerSalary = document.createElement("input");
  workerSalary.setAttribute("type", "text");
  workerSalary.setAttribute("name", "workerSalary");
  workerSalary.setAttribute("placeholder", "Worker Salary");
  workerSalary.setAttribute("id", "workerSalary");


  const worker_orders = document.createElement("input");
  worker_orders.setAttribute("type", "number");
  worker_orders.setAttribute("value", "worker_orders");
  worker_orders.setAttribute("placeholder", "Worker Orders");
  worker_orders.setAttribute("id", "worker_orders");


  const submit = document.createElement("input");
  submit.setAttribute("type", "submit");
  submit.setAttribute("value", "Submit");

  form.appendChild(workerId);
  form.appendChild(workerName);
  form.appendChild(workerUsername);
  form.appendChild(workerPassword);
  form.appendChild(workerSalary);
  form.appendChild(worker_orders);
  form.appendChild(submit);


  document.body.appendChild(form);

  form.addEventListener("submit", async (e) => {

    e.preventDefault();
    const workerid = document.getElementById("workerId").value;
    const workername = document.getElementById("workerName").value;
    const workerusername = document.getElementById("workerUsername").value;
    const workerpassword = document.getElementById("workerPassword").value;
    const workersalary = document.getElementById("workerSalary").value;
    const workerorders = document.getElementById("worker_orders").value;

    if (workerUsername==" ")
          {
            document.getElementById("workerUsername").innerHTML="";
              return;
            } 
    
    try{
      fetch("http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/createWorker", {
      method: "POST",
      headers: new Headers({
        "ngrok-skip-browser-warning": "3200",
        'contentType': 'application/json',
        charset: 'utf-8',
      }),

      body: JSON.stringify({
        'workerId': workerid,
        'workerName': workername,
        'workerUsername': workerusername,
        'workerpassword': workerpassword,
        'workerSalary': workersalary,
       ' worker_orders': workerorders
     

      }),

    });}
    catch(e){
      console.log(e);
    }
    
    
    try{
      fetch("http://b0c6-2401-4900-1fe7-4f5a-a09a-30ed-7197-8f93.ngrok.io/admin/updateWorker", {
      method: "PUT",
      headers: new Headers({
        "ngrok-skip-browser-warning": "3200",
        'contentType': 'application/json',
        charset: 'utf-8',
      }),

      body: JSON.stringify({
        'workerId': workerid,
        'workerName': workername,
        'workerUsername': workerusername,
        'workerpassword': workerpassword,
        'workerSalary': workersalary,
       ' worker_orders': workerorders
     

      }),
      
    });


  }
  catch(e){
    console.log(e);
  }
});
  
  
  
});





