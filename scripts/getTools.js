const tHead = ["toolId", "toolName", "toolSize", "toolPrice"];
const toolId = document.getElementById('toolId');
const toolName = document.getElementById('toolName');
const toolSize = document.getElementById('toolSize');
const toolPrice = document.getElementById('toolPrice');

const toolsTable = (token) => {
    const table = document.createElement('table');
    table.className = 'table';

    const tableHead = document.createElement('thead');
    tableHead.className = 'tableHead';

    const tableHeadRow = document.createElement('tr');
    tableHeadRow.className = 'tableHeadRow';

    tHead.forEach(header => {
        const currentHeader = document.createElement('th');
        currentHeader.innerText = header;
        tableHeadRow.append(currentHeader);
    });
    tableHead.append(tableHeadRow);
    table.append(tableHead);

    const tableBody = document.createElement('tbody');
    tableBody.className = "table-Body";
    table.append(tableBody);
    document.body.append(table);


    const getTools = async () => {
        try {
            let response = await fetch("https://e18b-103-169-241-128.in.ngrok.io/admin/getTools", {
                method: "get",
                headers: new Headers({
                    "Authorization":"Bearer "+token,
                    "ngrok-skip-browser-warning": "3200",
                    'Content-Type': 'application/json',
                    'charset': 'utf-8',
                })
            });
            let data = await response.json();
            console.log(data);
            result(data,token);
        }
        catch (error) {
            console.log(error);
        }

    }
    getTools();

    const result = (data) => {

        let template = "";
        const placeholder = document.getElementById("tbody");
        for (let index = 0; index < data.length; index++) {
            template +=
                `
                         <h1>TOOLS MANAGEMENT SYSTEM</h1> 
                         <tr>
                        <td> ${data[index].toolId} </td>
                        <td> ${data[index].toolName} </td>
                        <td> ${data[index].toolSize} </td>
                        <td> ${data[index].toolPrice} </td>
                         <td><button class="editButton">Edit</button>
                         <button class="deleteButton">Delete</button></td>
                         <tr>
     
     `
        }
        console.log(template);
        placeholder.innerHTML = template;

    }


}
// toolsTable();

export default toolsTable();










