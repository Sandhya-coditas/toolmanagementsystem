import workersTable from "./getWorkers.js";
import ordersTable from "./getOrders.js";
import toolsTable from "./getTools.js";

const displayDashboard=(token)=>{
        let template =
            `
                    <section>
                    <h1>ADMIN DASH BOARD</h1>
                     <button class="workersButton">Workers</button>
                     <button class="ordersButton">Orders</button>
                     <button class="toolsButton">Tools</button>
                     </section>

                     ` 


                     
document.getElementsByClassName("main")[0].innerHTML=template;

const workers=document.querySelector("workersButton");
// console.log(workers);
const orders=document.querySelector("ordersButton");
const tools=document.querySelector("toolsButton");


workers.addEventListener("click", async (e) => {
        e.preventDefault();
        workersTable['workersTable'](token);
})
tools.addEventListener("click", async (e) => {
        e.preventDefault();
        ordersTable(token);
})
orders.addEventListener("click", async (e) => {
        e.preventDefault();
        console.log("table printing");
        toolsTable(token);
});
}

export default displayDashboard();
