const tHead = ["workerId", "workerName", "workerUsername", "workerPassword", "workerSalary", "worker_orders"];
const workerId = document.getElementById('workerId');
const workerName = document.getElementById('workerName');
const workerUsername = document.getElementById('workerUsername');
const workerPassword = document.getElementById('workerPassword');
const workerSalary = document.getElementById('workerSalary');
const worker_orders = document.getElementById('worker_orders');
const workersTable = (token) => {
    console.log("hi");
    const table = document.createElement('table');
    table.className = 'table';

    const tableHead = document.createElement('thead');
    tableHead.className = 'tableHead';

    const tableHeadRow = document.createElement('tr');
    tableHeadRow.className = 'tableHeadRow';

    tHead.forEach(header => {
        const currentHeader = document.createElement('th');
        currentHeader.innerText = header;
        tableHeadRow.append(currentHeader);
    });
    tableHead.append(tableHeadRow);
    table.append(tableHead);

    const tableBody = document.createElement('tbody');
    tableBody.className = "table-Body";
    table.append(tableBody);
    document.body.append(table);


    const getWorkers = async () => {
        try {
            let response = await fetch("http://4a0a-103-97-165-115.ngrok.io/admin/getWorkers", {
                method: "GET",
                headers: new Headers({
                    "Authorization":"Bearer "+token,
                    "ngrok-skip-browser-warning": "3200",
                    'Content-Type': 'application/json',
                    'charset': 'utf-8',
                })
            });
            let data = await response.JSON();
            console.log(data);
            console.log('hrtr');
            result(data);
        }
        catch (error) {
            console.log(error);
        }

    }
    getWorkers();

    const result = (data) => {

        let template = "";
        const placeholder = document.getElementById("tbody");


        for (let index = 0; index < data.length; index++) {
            template +=
                `
                         <h1>WORKERS MANAGEMENT SYSTEM</h1>
                         <tr>
                         <td> ${data[index].workerId} </td>
                         <td> ${data[index].workerName} </td>
                         <td> ${data[index].workerUsername} </td>
                         <td> ${data[index].workerPassword} </td>
                         <td> ${data[index].workerSalary} </td>
                         <td> ${data[index].worker_orders} </td>
                         <td><button class="editButton">Edit</button>
                         <button class="deleteButton">Delete</button></td>
                         </tr>
     `
        }
        console.log(template);
        placeholder.innerHTML = template;

    }
}

export default {workersTable}









